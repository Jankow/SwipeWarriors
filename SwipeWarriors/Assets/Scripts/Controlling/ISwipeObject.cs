using UnityEngine;

public interface ISwipeObject
{
    void Swipe(Vector2 direction, float speed);
    Transform GetTransform();
    void ChangeColor(Color color);
    bool IsAbleToChoose();
    float MaxSwipeDistance { get; set; }
}

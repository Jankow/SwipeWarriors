public interface IControllingCharacter
{
    void DisableControlling();
    void EnableControlling();
}

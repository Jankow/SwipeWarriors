﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLayer: MonoBehaviour, IOnMyTurnStart, IOnMyTurnEnd
{
    private SpriteRenderer spriteRenderer;
    private Renderer myRenderer;

    private void Awake()
    {
        myRenderer = GetComponent<Renderer>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void IOnMyTurnEnd.OnMyTurnEnd()
    {
        if(spriteRenderer != null)
            spriteRenderer.sortingOrder = 4;

        myRenderer.material.SetColor("_Color", Color.red);
    }

    void IOnMyTurnStart.OnMyTurnStart()
    {
        if(spriteRenderer != null)
            spriteRenderer.sortingOrder = 5;

        //print("My turn Starts! " + gameObject.name);
        myRenderer.material.SetColor("_Color", Color.white);

    }
}

public interface ITeamConstraints
{
    void DisableCharacterChanging();
    void DisableCharactersActions();
}

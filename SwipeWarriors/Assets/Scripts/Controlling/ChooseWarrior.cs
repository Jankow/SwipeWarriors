using UnityEngine;

public class ChooseWarrior: MonoBehaviour
{
    [SerializeField]
    private Swipe swipe;

    void Update()
    {
        if(Input.GetMouseButtonDown(1))
        {
            RaycastHit hitInfo = new RaycastHit();
            Vector3 screenPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            Ray rayToPlayerPos = Camera.main.ScreenPointToRay(screenPos);
            int layermask = (int)(1 << 8);



            Debug.Log("clicked");
            RaycastHit2D hit = Physics2D.Raycast(transform.localPosition, Camera.main.ScreenToWorldPoint(Input.mousePosition));


            if(hit != null && hit.collider != null)
            {
                Debug.Log("collider is not null");

                Debug.Log(hit.collider.gameObject.name);

                ISwipeObject swipeObject = hit.collider.gameObject.GetComponent<ISwipeObject>();
                if(swipeObject != null && swipeObject.IsAbleToChoose())
                {
                    Debug.Log("Selecting warrior");
                    swipe.SetSwipeObject(swipeObject);
                }
                else
                    Debug.Log("Kupa inna scena i nie ma skryptu dobrego.");
            }
        }
    }
}

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public interface ISwipeTrajectory
{
    void CreateTrajectory(Vector2 startPosition, Vector2 velocityVector, float mass);
    void CreateTrajectory(Rigidbody2D body, Vector2 velocityVector);
    void ClearTrajectory();
}

public class Swipe : MonoBehaviour, IOnTurnEnd, IControllingCharacter, IOnTurnStart
{
    private Vector2 startPosition, endPosition;
    private ISwipeObject swipeObject;
    private WaitForFixedUpdate waitFrame = new WaitForFixedUpdate();
    [SerializeField] SwipePivot swipePivot;

    public ISwipeTrajectory Trajectory
    {
        get; set;
    }

    public bool IsAbleToControlAnything
    {
        get; set;
    }

    public void SetSwipeObject(ISwipeObject obj)
    {
        if (swipeObject != null)
            swipeObject.ChangeColor(Color.white);

        this.swipeObject = obj;
        swipeObject.ChangeColor(Color.yellow);
    }

    public void SwipeObject(Vector2 direction, float power)
    {
        if (swipeObject != null)
        {
            if (power > 6)
            {
                swipeObject.Swipe(direction, power);
            }
            else
            {
                print("too low swipe");
            }
        }
    }

    public void CleanChoosedObject()
    {
        swipeObject = null;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && swipeObject != null)
        {
            if (IsAbleToControlAnything)
                StartCoroutine(CheckForSwipe());
        }
    }

    private IEnumerator CheckForSwipe()
    {
        Vector2 startPointerPosition = Input.mousePosition;
        WaitForFixedUpdate waitFrame = new WaitForFixedUpdate();
        bool distanceDone = true;
        do
        {
            if (Input.GetMouseButtonUp(0))
            {
                distanceDone = false;
                break;
            }

            yield return waitFrame;
        } while (Vector2.Distance(startPointerPosition, Input.mousePosition) < 5f);

        if (distanceDone)
            StartCoroutine(Aiming());
    }

    private IEnumerator Aiming()
    {
        swipePivot.PlaceSwipePivot();

        startPosition = Camera.main.ViewportToScreenPoint(Input.mousePosition);
        Vector2 direction = new Vector2();
        float power = 0;

        while (Input.GetMouseButton(0))
        {
            endPosition = Camera.main.ViewportToScreenPoint(Input.mousePosition);
            direction = -(endPosition - startPosition);
            power = direction.magnitude;
            direction.Normalize();
#if UNITY_EDITOR
            power /= 8000f;
#else
            power /= 100000f;
#endif
            power = Mathf.Min(swipeObject.MaxSwipeDistance, power);
            swipePivot.DecidePivotColor(power);
            //Trajectory.CreateTrajectory(swipeObject.GetTransform().position, direction * power, 1);
            Trajectory.CreateTrajectory(swipeObject.GetTransform().GetComponent<Rigidbody2D>(), direction * power);
            StopControllingWhenBlocked();
            yield return waitFrame;
        }
        swipePivot.DestroyPivot();
        Trajectory.ClearTrajectory();
        SwipeObject(direction, power);
    }



    private void StopControllingWhenBlocked()
    {
        if (!IsAbleToControlAnything)
        {
            StopCoroutine(Aiming());
            Trajectory.ClearTrajectory();
        }
    }

    private void ClearCurrentSwipeObject()
    {
        if (swipeObject != null)
        {
            swipeObject.ChangeColor(Color.white);
            swipeObject = null;
        }
    }

    void IOnTurnEnd.OnTurnEnd()
    {
        ClearCurrentSwipeObject();
    }

    void IControllingCharacter.DisableControlling()
    {
        IsAbleToControlAnything = false;
    }

    void IControllingCharacter.EnableControlling()
    {
        IsAbleToControlAnything = false;
    }

    void IOnTurnStart.OnTurnStart()
    {
        IsAbleToControlAnything = true;
    }


}

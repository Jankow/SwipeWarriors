using System.Collections.Generic;
using UnityEngine;

public class TeamConstraintsHandler: MonoBehaviour, IUpdateMember, ITeamConstraints
{
    private List<ITeamConstraintsUser> teamConstraintUsers = new List<ITeamConstraintsUser>();
    private List<IControllingCharacter> controllingCharacters = new List<IControllingCharacter>();

    void IUpdateMember.Clean()
    {
        teamConstraintUsers.Clear();
        controllingCharacters.Clear();
    }

    void ITeamConstraints.DisableCharacterChanging()
    {
        foreach(var x in controllingCharacters)
        {
            x.DisableControlling();
        }
    }

    void ITeamConstraints.DisableCharactersActions()
    {
        foreach(var x in controllingCharacters)
        {
            x.DisableControlling();
        }
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IControllingCharacter)
        {
            controllingCharacters.Add(mono as IControllingCharacter);
        }

        if(mono is ITeamConstraintsUser)
        {
            teamConstraintUsers.Add(mono as ITeamConstraintsUser);
            teamConstraintUsers[teamConstraintUsers.Count - 1].GiveTeamConstraints(this);
        }
    }
}

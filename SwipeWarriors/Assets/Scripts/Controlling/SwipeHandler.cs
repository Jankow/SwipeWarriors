using System.Collections.Generic;
using UnityEngine;

public class SwipeHandler: MonoBehaviour, IUpdateMember
{
    [SerializeField] private Swipe swipe;

    private List<ISwipeUser> swipeUsers = new List<ISwipeUser>();

    void IUpdateMember.Clean()
    {
        swipeUsers.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is ISwipeUser)
        {
            swipeUsers.Add(mono as ISwipeUser);
            swipeUsers[swipeUsers.Count - 1].GiveSwipe(swipe);
        }

    }
}

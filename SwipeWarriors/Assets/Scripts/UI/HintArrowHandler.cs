using UnityEngine;

public class HintArrowHandler: MonoBehaviour
{
    [SerializeField] private GameObject arrowObject;

    [Range(0, 100)]
    [SerializeField]
    private float arrowExtendMultiplier;

    public void PlaceArrow(Vector2 heroPosition, Vector2 direction, float power)
    {
        arrowObject.SetActive(true);
        PlaceArrowAtHero(heroPosition);
        SetArrowRotation(direction);
        ExtendArrow(power);
    }

    public void HideArrow()
    {
        arrowObject.SetActive(false);
    }

    private float GetMultiplierOfXScaleByPower(float power)
    {
        return power * arrowExtendMultiplier;
    }

    private void ExtendArrow(float power)
    {
        arrowObject.transform.localScale = new Vector3(GetMultiplierOfXScaleByPower(power), 1f, 1f);
    }

    private void SetArrowRotation(Vector2 direction)
    {
        float angle = Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI;
        Vector3 arrowRotation = arrowObject.transform.eulerAngles;
        arrowObject.transform.eulerAngles = new Vector3(arrowRotation.x, arrowRotation.y, angle);
    }

    private void PlaceArrowAtHero(Vector2 heroPosition)
    {
        arrowObject.transform.position = heroPosition;
    }
}

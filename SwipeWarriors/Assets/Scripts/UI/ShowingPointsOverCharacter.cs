﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShowingPointsOverCharacter : MonoBehaviour {

    [SerializeField] private GameObject textTemplate;

    public void ShowPoints(int value, Color color)
    {
        GameObject textObject = Instantiate(textTemplate, transform);
        textObject.transform.SetAsLastSibling();
        Text text = textObject.GetComponent<Text>();
        text.text = value.ToString();
        text.color = color;

        text.DOFade(0f, 1f);
        textObject.GetComponent<RectTransform>().DOAnchorPosY(0.7f, 1f).OnComplete(() => { Destroy(textObject); });
    }
}
 
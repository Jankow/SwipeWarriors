﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapClickChoice : MonoBehaviour
{
    [SerializeField] private GameObject mapObject;
    [SerializeField] private string sceneName;

    public void MapClick()
    {
        mapObject.GetComponent<Image>().sprite = gameObject.GetComponent<Image>().sprite;
        mapObject.GetComponent<ChosenMap>().Choose(sceneName);
    }
}

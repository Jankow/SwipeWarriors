﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameInfo : MonoBehaviour
{
    private static bool created = false;
    private static List<GameObject>[] chosenHeroes = new List<GameObject>[2];

    [SerializeField] private List<GameObject> prefabs = new List<GameObject>();

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            Debug.Log("Awake: " + this.gameObject);
            chosenHeroes[0] = new List<GameObject>();
            chosenHeroes[1] = new List<GameObject>();
            //created = true;
        }
    }

    void OnLevelWasLoaded()
    {
        Debug.Log("Spawning heroes.");

        // Here code of searching for where to put teams
        List<GameObject> spawn = GameObject.FindGameObjectsWithTag("HeroSpawn").ToList();
        if (spawn.Count > 0)
        {
            GameObject[] heroes = GameObject.FindGameObjectsWithTag("Warrior");
            foreach (var hero in heroes)
            {
                Destroy(hero);
            }

            int playerNum = 0, i = 0;
            foreach (var chosenHeroesForPlayer in chosenHeroes)
            {
                foreach (var chosenHero in chosenHeroesForPlayer)
                {
                    int val = Random.Range(0, spawn.Count);
                    GameObject hero = Instantiate(chosenHero, spawn[val].transform.position, Quaternion.identity);
                    spawn.RemoveAt(val);
                    hero.GetComponent<NormalMan>().TeamID = playerNum;
                    i++;
                }
                playerNum++;
            }

            GameObject.FindObjectOfType<MainUpdateInterfacesHandler>().UpdateAllInterfaces();

            Destroy(gameObject);
        }
        else
        {
            Debug.Log("No spawns found.");
        }
    }

    public void AddHero(int player, ChosenHeroInfo chosenHeroInfo)
    {
        chosenHeroes[player].Add(prefabs[chosenHeroInfo.GetChosenHeroIndex()]);
        // Apply stats to that gameObject here from chosenHeroInfo
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChosenMap : MonoBehaviour
{
    public string CurrentChosenSceneToLoad = "";

    public void Choose(string sceneName)
    {
        CurrentChosenSceneToLoad = sceneName;
    }
}

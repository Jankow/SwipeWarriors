﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameButton : MonoBehaviour
{
    [SerializeField] private GameInfo gameInfo;

    [SerializeField] private ChosenMap chosenMap;

    [SerializeField] private List<ChosenHeroViewModel> chosenHeroesP1 = new List<ChosenHeroViewModel>();
    [SerializeField] private List<ChosenHeroViewModel> chosenHeroesP2 = new List<ChosenHeroViewModel>();

    public void ClickStartGame()
    {
        foreach (ChosenHeroViewModel chosenHeroViewModel in chosenHeroesP1)
        {
            gameInfo.AddHero(0, chosenHeroViewModel.GetModel());
        }
        foreach (ChosenHeroViewModel chosenHeroViewModel in chosenHeroesP2)
        {
            gameInfo.AddHero(1, chosenHeroViewModel.GetModel());
        }
        SceneManager.LoadScene(chosenMap.CurrentChosenSceneToLoad);
    }
}

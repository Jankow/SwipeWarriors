﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChosenHeroInfo
{
    private int _chosenHeroIndex;
    private ChoiceOfPosition chosenPosition = ChoiceOfPosition.A;

    public void ChangeChosenPosition(ChoiceOfPosition choiceOfPosition)
    {
        chosenPosition = choiceOfPosition;
    }

    public ChoiceOfPosition GetChosenPosition()
    {
        return chosenPosition;
    }

    public void SetChosenHero(int chosenHeroIndex)
    {
        this._chosenHeroIndex = chosenHeroIndex;
    }

    public int GetChosenHeroIndex()
    {
        return _chosenHeroIndex;
    }
}

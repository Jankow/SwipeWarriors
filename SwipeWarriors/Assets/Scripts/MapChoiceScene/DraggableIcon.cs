﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DraggableIcon : MonoBehaviour
{
    public int heroIndexToSet = 0;

    private void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Kolizja z " + col.gameObject.name);
        col.gameObject.GetComponentInChildren<Image>().sprite = gameObject.GetComponent<Image>().sprite;
        col.gameObject.GetComponentInChildren<ChosenHeroViewModel>().SetChosenHero(heroIndexToSet);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollRectWithButton : MonoBehaviour
{
    private ScrollRect scrollRect;
    private float increment = 0;
    private float numberOfChildren;

    void Awake()
    {
        scrollRect = gameObject.GetComponent<ScrollRect>();
        numberOfChildren = scrollRect.transform.GetChild(0).transform.childCount - 2;
    }

    public void Scroll(int velocity)
    {
        if (velocity > 0 && increment > 0)
            increment--;
        if (velocity < 0 && increment < numberOfChildren)
            increment++;
        scrollRect.horizontalNormalizedPosition = increment / numberOfChildren;
    }
}

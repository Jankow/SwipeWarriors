﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChosenHeroViewModel : MonoBehaviour
{
    private ChosenHeroInfo heroInfo = new ChosenHeroInfo();

    public Text ChosenPositionText;

    public void ChangeChosenLane()
    {
        ChoiceOfPosition chosenPosition = heroInfo.GetChosenPosition();
        if (chosenPosition == ChoiceOfPosition.A)
            chosenPosition = ChoiceOfPosition.B;
        else if (chosenPosition == ChoiceOfPosition.B)
            chosenPosition = ChoiceOfPosition.C;
        else if (chosenPosition == ChoiceOfPosition.C)
            chosenPosition = ChoiceOfPosition.A;
        heroInfo.ChangeChosenPosition(chosenPosition);

        ChosenPositionText.text = chosenPosition.ToString();
    }

    public void SetChosenHero(int heroIndex)
    {
        heroInfo.SetChosenHero(heroIndex);
    }

    public ChosenHeroInfo GetModel()
    {
        return heroInfo;
    }
}

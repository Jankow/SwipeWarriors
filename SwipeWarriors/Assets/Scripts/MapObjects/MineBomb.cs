﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineBomb : MonoBehaviour, ITurnOrderUser
{
    [SerializeField]
    private int damage;
    [SerializeField]
    private int powerOfExplosion;
    [SerializeField]
    private Collider2D mineBombTrigger;

    private ITurnOrder turnOrder;

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        ExecuteMineBomb();
    }

    private void ExecuteMineBomb()
    {
        Collider2D[] colliders = new Collider2D[30];
        mineBombTrigger.GetContacts(colliders);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i] != null && colliders[i].tag == "Warrior")
            {
                WarriorsCustomPhysics customPhysics = colliders[i].GetComponent<WarriorsCustomPhysics>();
                if (customPhysics != null)
                {
                    IDealDamage dealDamage = colliders[i].transform.parent.GetComponent<IDealDamage>();
                    ITeamID teamID = colliders[i].GetComponentInParent<ITeamID>();
                    if (dealDamage != null && teamID.TeamID != turnOrder.GetCurrentTeam().TeamID)
                    {
                        dealDamage.Damage(damage);
                    }


                    Debug.Log("THrowing opoonntt!!");
                    Vector2 targetPosition = colliders[i].transform.position;
                    Vector2 currentPosition = transform.position;
                    customPhysics.ThrowWarrior(powerOfExplosion * (targetPosition - currentPosition).normalized);
                }
            }
        }
        Destroy(gameObject);
    }
}

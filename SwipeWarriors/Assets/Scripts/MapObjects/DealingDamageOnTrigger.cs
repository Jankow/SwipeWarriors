﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealingDamageOnTrigger : MonoBehaviour, ITurnOrderUser
{
    private Rigidbody2D body;
    private ITurnOrder turnOrder;

    public Func<int> onDamage;

    private void Awake()
    {
        body = transform.parent.GetComponent<Rigidbody2D>();
        onDamage = GetDefaultDamage;
    }

    private int GetDefaultDamage()
    {
        return Mathf.RoundToInt(body.velocity.magnitude);
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Warrior")
        {
            IDealDamage dealDamage = collision.transform.parent.GetComponent<IDealDamage>();
            ITeamID teamID = collision.GetComponentInParent<ITeamID>();
            if (dealDamage != null && teamID.TeamID != turnOrder.GetCurrentTeam().TeamID)
            {
                dealDamage.Damage(onDamage());
            }
        }
    }
}

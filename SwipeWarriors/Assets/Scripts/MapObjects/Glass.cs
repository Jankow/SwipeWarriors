﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glass : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Warrior")
        {
            Rigidbody2D body = collision.transform.parent.GetComponent<Rigidbody2D>();
            if (body.velocity.magnitude * body.mass > 5f)
            {
                Destroy(gameObject);
            }
        }
    }
}

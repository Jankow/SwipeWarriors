﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectOnTargetPhysics : MonoBehaviour {

    private Rigidbody2D body;
    private IEnumerator calculateEnemyReboundOnTheEndOfPhysics;
    private Action actionOnTargetInTheEndOfFrame;
    private bool firstFrameAfterShoot;

    void Awake()
    {
        body = transform.parent.GetComponent<Rigidbody2D>();
        StartCoroutine(CalculateEnemyReboundOnTheEndOfPhysics());
    }

    private IEnumerator CalculateEnemyReboundOnTheEndOfPhysics()
    {
        WaitForFixedUpdate waitForFixed = new WaitForFixedUpdate();
        while (true)
        {
            yield return waitForFixed;
            if (actionOnTargetInTheEndOfFrame != null)
            {
                actionOnTargetInTheEndOfFrame();
                actionOnTargetInTheEndOfFrame = null;
            }
        }
    }

    public void WaitForEndOfFrameAndDisableCollisionStay()
    {
        firstFrameAfterShoot = true;
        StartCoroutine(WaitForFrame());
    }

    private IEnumerator WaitForFrame()
    {
        yield return new WaitForFixedUpdate();
        firstFrameAfterShoot = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (firstFrameAfterShoot)
        {
            print("In first frame collision " + gameObject.name + " " + body.velocity);
            ExecutePhysics(collision);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        ExecutePhysics(other);
    }

    private void ExecutePhysics(Collider2D other)
    {
        if (other.transform.tag == "Warrior")
        {
            Vector2 result = GetReboundVector(other);
            PhysicsActionOnEnemy(other, result);
        }
    }

    private Vector2 GetReboundVector(Collider2D other)
    {
        Vector2 targetPos = other.transform.position;
        Vector2 directionBetweenCollision = targetPos - (Vector2)transform.position;
        Vector2 velocityDirection = body.velocity.normalized;
        float cosAngle = GetCosinusBetween2Vectors(velocityDirection, directionBetweenCollision);

        return directionBetweenCollision * body.velocity.magnitude * cosAngle;
    }

    private void PhysicsActionOnEnemy(Collider2D other, Vector2 direction)
    {
        Rigidbody2D enemyBody = other.transform.parent.GetComponent<Rigidbody2D>();
        float physicsCollisionMultiplier = body.mass / (body.mass + enemyBody.mass) * 2f;

        IThrowObject throwObject = other.transform.parent.GetComponent<IThrowObject>();
        actionOnTargetInTheEndOfFrame = () =>
        {
            throwObject.HitObject(body.gameObject, direction.normalized, direction.magnitude * physicsCollisionMultiplier);
        };
    }

    private float GetCosinusBetween2Vectors(Vector2 a, Vector2 b)
    {
        float result = a.x * b.x +
                       a.y * b.y;

        float lengthA = Mathf.Sqrt(Mathf.Pow(a.x, 2) + Mathf.Pow(a.y, 2));
        float lengthB = Mathf.Sqrt(Mathf.Pow(b.x, 2) + Mathf.Pow(b.y, 2));
        if (lengthA == 0)
            lengthA = 1;
        if (lengthB == 0)
            lengthB = 1;

        result /= lengthA * lengthB;

        return result;
    }
}

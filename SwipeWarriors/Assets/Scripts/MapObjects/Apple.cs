﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apple : MonoBehaviour {

    [SerializeField]
    private int healingValue;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Warrior")
        {
            collision.transform.parent.GetComponent<CharacterHealth>().AddHealth(healingValue);
            Destroy(gameObject);
        }
    }
}

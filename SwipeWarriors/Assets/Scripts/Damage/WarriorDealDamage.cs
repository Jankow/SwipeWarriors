﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorDealDamage : MonoBehaviour, ITurnOrderUser, IOnBeingSwiped
{
    public Func<int> dealDamage { get; set; }
    public bool IsAbleToDamageTurnOwner { get; set; }
    public bool FriendlyFire { get; set; }

    private Character currentCharacter;
    private Rigidbody2D body;
    private ITurnOrder turnOrder;
    private CharacterDamage characterDamage;
    private bool firstFrameAfterShoot;

    void Awake()
    {
        body = transform.parent.GetComponent<Rigidbody2D>();
        dealDamage = DefaultDamageDealing;
        currentCharacter = transform.parent.GetComponent<Character>();
        characterDamage = transform.parent.GetComponent<CharacterDamage>();
    }

    private int DefaultDamageDealing()
    {
        return Mathf.RoundToInt(body.velocity.magnitude * characterDamage.DamageMultiplier);
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageOnWarrior(collision);
    }

    private void DamageOnWarrior(Collider2D collision)
    {
        if (collision.tag == "Warrior")
        {
            Character hittingCharacter = collision.transform.parent.GetComponent<Character>();
            if (hittingCharacter != null)
            {
                int targetTeamID = hittingCharacter.TeamID;
                if (!FriendlyFire)
                {
                    if (currentCharacter.TeamID == targetTeamID)
                        return;
                }

                if (!IsAbleToDamageTurnOwner)
                {

                    int currentTeamID = turnOrder.GetCurrentTeam().TeamID;
                    if (currentTeamID == targetTeamID)
                        return;
                }
                else
                {
                    print("Dealing damage to turn owner");
                }
                //hittingCharacter.GetComponent<IDealDamage>().Damage(Mathf.RoundToInt(body.velocity.magnitude));
                hittingCharacter.GetComponent<IDealDamage>().Damage(dealDamage());
            }
        }
    }

    private void WaitForEndOfFrameAndDisableCollisionStay()
    {
        firstFrameAfterShoot = true;
        StartCoroutine(WaitForFrame());
    }

    private IEnumerator WaitForFrame()
    {
        yield return new WaitForFixedUpdate();
        firstFrameAfterShoot = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (firstFrameAfterShoot)
        {
            DamageOnWarrior(collision);

        }
    }

    void IOnBeingSwiped.CharacterSwiped(Vector2 power)
    {
        WaitForEndOfFrameAndDisableCollisionStay();
    }
}

using UnityEngine;

public class NormalMan: Hero
{
    [SerializeField] private int startingTeamID;
    private int baseMass = 40;
    private int baseSpeed = 60;

    void Awake()
    {
        TeamID = startingTeamID;
        Mass = baseMass;
        Speed = baseSpeed;
    }

    public override void TakesTurn()
    {

    }

    public override void SomeAbilityFunction()
    {
        // No Ability - this is just a Normal Man
    }
}

using UnityEngine;

public abstract class Character: MonoBehaviour, ITeamID, IOnDestroy
{

    public int Mass
    {
        get; set;
    }
    public int Speed
    {
        get; set;
    }

    public int TeamID
    {
        get;set;
    }

    public abstract void TakesTurn();
    
    void IOnDestroy.NotifyOnDestroy()
    {
        GameObject.FindObjectOfType<DestroyNotifier>().Notify(this);
    }
}

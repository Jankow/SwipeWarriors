﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Hero : Character
{
    
    public abstract void SomeAbilityFunction();
}

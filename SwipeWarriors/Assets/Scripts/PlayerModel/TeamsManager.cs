using System.Collections.Generic;
using UnityEngine;

public class TeamsManager: MonoBehaviour, IPlayersList
{
    private List<Team> teamsList = new List<Team>();
    [SerializeField] private GameObject teamTemplate;
    void Start()
    {
        FillTeamsList();
    }

    private void FillTeamsList()
    {
        foreach(var item in FindObjectsOfType<MonoBehaviour>())
        {
            if(item is Character)
            {
                Character warriorScript = item.gameObject.GetComponent<Character>();
                if(teamsList.Find((t) => t.TeamID == warriorScript.TeamID) == null)
                {
                    GameObject newTeam = Instantiate(teamTemplate);
                    Team team = newTeam.GetComponent<Team>();
                    team.TeamID = warriorScript.TeamID;
                    teamsList.Add(team);
                }

                teamsList.Find(t => t.TeamID == warriorScript.TeamID).AddCharacter(warriorScript);
            }
        }
    }

    public void RemoveCharacter(Character character)
    {
        teamsList.Find(t => t.TeamID == character.TeamID).RemoveCharacter(character);
    }

    public List<Team> GetTeams()
    {
        return teamsList;
    }

    public Team GetTeam(int index)
    {
        return teamsList.Find(t => t.TeamID == index);
    }

    List<Character> IPlayersList.GetCharacters()
    {
        List<Character> characters = new List<Character>();
        foreach(var team in teamsList)
        {
            characters.AddRange(team.GetCharacters());
        }
        return characters;
    }
}

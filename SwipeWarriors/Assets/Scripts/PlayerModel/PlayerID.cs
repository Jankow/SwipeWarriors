using UnityEngine;

public class PlayerID: MonoBehaviour
{
    public int playerNumber
    {
        get; set;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITeamID {
    int TeamID { get; set; }
}

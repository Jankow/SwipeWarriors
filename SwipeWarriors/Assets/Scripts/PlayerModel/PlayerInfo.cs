public class PlayerInfo
{
    private readonly string _playerName;
    public string PlayerName
    {
        get
        {
            return _playerName;
        }
    }

    public PlayerInfo(string playerName)
    {
        this._playerName = playerName;
    }
}

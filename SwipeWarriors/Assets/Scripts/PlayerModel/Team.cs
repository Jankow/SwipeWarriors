using System.Collections.Generic;
using UnityEngine;

public class Team : MonoBehaviour, IOnMyTeamTurnEnd, ITeamID
{
    public PlayerID PlayerData
    {
        get; set;
    }

    private List<Character> charactersList;
    private int currentCharacterIndex = 0;
    private bool firstTurnInGame;

    private void Awake()
    {
        firstTurnInGame = true;
        charactersList = new List<Character>();
    }

    public Character CurrentPlayingCharacter
    {
        get
        {
            Character currentCharacter = null;
            do
            {
                if (firstTurnInGame)
                {
                    firstTurnInGame = false;
                    currentCharacterIndex = Random.Range(0, charactersList.Count);
                }

                if (charactersList.Count == 0)
                    return null;
                if (currentCharacterIndex >= charactersList.Count)
                    currentCharacterIndex = 0;

                currentCharacter = charactersList[currentCharacterIndex];

                if (currentCharacter == null)
                    charactersList.RemoveAt(currentCharacterIndex);
            }
            while (currentCharacter == null);
            return currentCharacter;
        }
    }

    public int TeamID
    {
        get; set;
    }

    public Team(int id)
    {
        TeamID = id;
    }

    public List<Character> GetCharacters()
    {
        int destroyCount = 0;

        for (int i = 0; i < charactersList.Count; i++)
        {
            if (charactersList[i - destroyCount] == null)
            {
                charactersList.RemoveAt(i - destroyCount);
                destroyCount++;
            }
        }

        return charactersList;
    }

    public void RemoveCharacter(Character character)
    {
        charactersList.Remove(character);
    }

    public void AddCharacter(Character character)
    {
        charactersList.Add(character);
    }

    void IOnMyTeamTurnEnd.OnMyTeamTurnEnd()
    {
        if (charactersList.Count > 0)
            currentCharacterIndex = (currentCharacterIndex + 1) % charactersList.Count;
    }

}

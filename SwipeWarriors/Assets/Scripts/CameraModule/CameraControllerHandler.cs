﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllerHandler : MonoBehaviour, IUpdateMember
{
    [SerializeField] private CameraController cameraController;
    private List<ICameraControllerUser> cameraControllerUsers = new List<ICameraControllerUser>();

    void IUpdateMember.Clean()
    {
        cameraControllerUsers.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if (mono is ICameraControllerUser && !cameraControllerUsers.Contains(mono as ICameraControllerUser))
        {
            cameraControllerUsers.Add(mono as ICameraControllerUser);
            cameraControllerUsers[cameraControllerUsers.Count - 1].GiveICameraController(cameraController);
        }
    }
}

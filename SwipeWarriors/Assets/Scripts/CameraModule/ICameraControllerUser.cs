﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICameraControllerUser  {
    void GiveICameraController(ICameraController cameraController);
}

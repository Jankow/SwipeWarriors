﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class CameraController : MonoBehaviour, ICameraController
{
    [SerializeField] private Camera mainGameCamera;

    private bool isPriorityTaken;
    private MonoBehaviour whoHasPriorityOnCamera;
    bool ICameraController.IsPriorityTaken { get { return isPriorityTaken; } }

    private Tween cameraMove;
    private Tween cameraSizeChange;

    void ICameraController.SetBaseVisibility(MonoBehaviour invoker, Boundaries boundaries)
    {
        TryToMoveCamera(invoker, boundaries.CenterOfBoundaries, boundaries.BoundariesRange);
    }
 
    void ICameraController.SetBaseVisibility(MonoBehaviour invoker, Vector2 point, float range)
    {
        TryToMoveCamera(invoker, point, range);

    }

    bool ICameraController.TakePriority(MonoBehaviour whoTakes)
    {
        if (!isPriorityTaken)
        {
            whoHasPriorityOnCamera = whoTakes;
            isPriorityTaken = true;
            return true;
        }

        return false;
    }

    bool ICameraController.UnlockPriority(MonoBehaviour whoUnlocks)
    {
        if (whoUnlocks == whoHasPriorityOnCamera || whoHasPriorityOnCamera == null)
        {
            isPriorityTaken = false;
            whoHasPriorityOnCamera = null;
            return true;
        }

        return false;
    }

    private void TryToMoveCamera(MonoBehaviour invoker, Vector2 centerOfBoundaries, float boundariesRange)
    {
        if (isPriorityTaken)
            if (invoker != whoHasPriorityOnCamera)
                return;

        if (cameraMove != null && cameraMove.IsPlaying())
            cameraMove.Kill();
        if (cameraSizeChange != null && cameraSizeChange.IsPlaying())
            cameraSizeChange.Kill();

        float range = Mathf.Max(12f, boundariesRange);
        Vector3 targetPoint = new Vector3(centerOfBoundaries.x, centerOfBoundaries.y, -10f);
        cameraMove = mainGameCamera.transform.DOMove(targetPoint, .8f);
        cameraSizeChange =  mainGameCamera.DOOrthoSize(range / 1.3f, .8f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICameraController
{
    bool TakePriority(MonoBehaviour whoTakes);
    bool UnlockPriority(MonoBehaviour whoUnlocks);
    bool IsPriorityTaken { get; }
    void SetBaseVisibility(MonoBehaviour invoker, Vector2 point, float range);
    void SetBaseVisibility(MonoBehaviour invoker, Boundaries boundaries);

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCameraFocus : MonoBehaviour, ICameraControllerUser, IOnMyTurnStart {
    private ICameraController cameraController;

    void ICameraControllerUser.GiveICameraController(ICameraController cameraController)
    {
        this.cameraController = cameraController;
    }

    void IOnMyTurnStart.OnMyTurnStart()
    {
        cameraController.SetBaseVisibility(this, transform.position, 12f);
    }
}

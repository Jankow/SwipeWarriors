﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOnHeroesFocusWhenBouncing : MonoBehaviour, ICameraControllerUser, IPlayersListUser, IOnEveryTurnStart, IOnEveryTurnEnd
{
    private ICameraController cameraController;
    private IPlayersList playersList;
    private List<Character> characters;
    private List<Rigidbody2D> charactersBodies;
    private Boundaries boundaries = new Boundaries();

    public void OnEveryTurnStart()
    {
        boundaries.ResetBounds();
        characters = playersList.GetCharacters();
        FillCharactersBodies();
        StartCoroutine(LookAtPlayers());
    }

    private void FillCharactersBodies()
    {
        charactersBodies = new List<Rigidbody2D>();
        foreach (var character in characters)
        {
            if (character != null)
            {
                Rigidbody2D body = character.GetComponent<Rigidbody2D>();

                if (body)
                {
                    charactersBodies.Add(body);
                }
            }

        }
    }

    private IEnumerator LookAtPlayers()
    {
        WaitForFixedUpdate waitFrame = new WaitForFixedUpdate();
        while (true)
        {
            boundaries.ResetBounds();
            CalculateBounds();
            if (boundaries.CenterOfBoundaries != new Vector2())
            {
                cameraController.TakePriority(this);
                cameraController.SetBaseVisibility(this, boundaries);
            }

            yield return waitFrame;
        }
    }

    private void CalculateBounds()
    {
        foreach (var body in charactersBodies)
        {
            if (body != null && body.velocity.magnitude > 1f)
            {
                boundaries.TryToChangeBounds(body.transform.position);
            }
        }
    }


    void ICameraControllerUser.GiveICameraController(ICameraController cameraController)
    {
        this.cameraController = cameraController;
    }

    void IPlayersListUser.GiveIPlayerList(IPlayersList playersList)
    {
        this.playersList = playersList;
    }

    void IOnEveryTurnEnd.OnEveryTurnEnd()
    {
        StopCoroutine(LookAtPlayers());
        cameraController.UnlockPriority(this);
    }
}


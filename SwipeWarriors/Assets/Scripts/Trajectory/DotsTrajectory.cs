using System;
using System.Collections.Generic;
using UnityEngine;

public class DotsTrajectory : MonoBehaviour, ISwipeTrajectory, ICameraControllerUser
{
    public int resolution = 10;

    [SerializeField] private GameObject dot;
    [SerializeField] private Transform trajectoryDotsParent;
    [SerializeField] private float distanceMultiplier;


    private List<GameObject> dots = new List<GameObject>();

    private Vector2 startPosition;
    private float velocity;
    private float angle;

    private float g;
    private float radianAngle;

    private ICameraController cameraController;
    private Boundaries boundaries = new Boundaries();

    void ICameraControllerUser.GiveICameraController(ICameraController cameraController)
    {
        this.cameraController = cameraController;
    }

    private void Awake()
    {
        transform.parent.GetComponent<Swipe>().Trajectory = this;
        g = Mathf.Abs(Physics2D.gravity.y);
    }

    private void RenderArc()
    {
        DeleteCurrentDots();
        Vector2[] points = CalculationsArray(distanceMultiplier);
        Vector2[] pointsForCameraView = CalculationsArray(1f);

        boundaries.ResetBounds();
        CalculateViewBounds(pointsForCameraView);
        boundaries.TryToChangeBounds(startPosition);

        cameraController.SetBaseVisibility(this, boundaries);
        CreateLine(points);
    }

    private void CalculateViewBounds(Vector2[] points)
    {

        foreach (var point in points)
        {
            boundaries.TryToChangeBounds(point);
        }
    }

    public void CreateTrajectory(Rigidbody2D body, Vector2 velocityVector)
    {
        this.startPosition = body.transform.position;

        DeleteCurrentDots();
        //Vector2[] points = CalculationsArray(distanceMultiplier);
        var points = Prediction.Plot(body, body.transform.position, velocityVector, 10);
        //Vector2[] pointsForCameraView = CalculationsArray(1f);

        boundaries.ResetBounds();
        CalculateViewBounds(points);
        boundaries.TryToChangeBounds(startPosition);

        cameraController.SetBaseVisibility(this, boundaries);
        CreateLine(points);


    }

    public void CreateTrajectory(Vector2 startPosition, Vector2 velocityVector, float mass)
    {
        // Debug.Log("Create trajectory " + startPosition + " " + velocityVector + " " + mass);
        //print(Vector2.Angle(new Vector2(1, 0), new Vector2(-0.6f, -0.7f)));

        angle = Vector2.Angle(new Vector2(1, 0), velocityVector);
        if (velocityVector.y < 0)
            angle += 180;

        velocity = velocityVector.magnitude / mass;
        this.startPosition = startPosition;
        RenderArc();
    }

    public void ClearTrajectory()
    {
        DeleteCurrentDots();
    }

    private void DeleteCurrentDots()
    {
        for (int i = 0; i < dots.Count; i++)
        {
            Destroy(dots[i]);
        }
        dots.Clear();
    }

    private void CreateLine(Vector2[] calculationsArray)
    {
        for (int i = 0; i < calculationsArray.Length - 1; i++)
        {
            GenerateDot(calculationsArray[i]);
        }
        GenerateDot(calculationsArray[calculationsArray.Length - 1]);
    }

    private void GenerateDot(Vector2 calculatedPosition)
    {
        if (!float.IsNaN(calculatedPosition.x) && !float.IsNaN(calculatedPosition.y))
            dots.Add(Instantiate(dot, calculatedPosition, Quaternion.identity));
    }

    private Vector2[] CalculationsArray(float howMuchTrajectoryShow)
    {
        Vector2[] arcArray = new Vector2[resolution + 1];
        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = (velocity * velocity * Mathf.Sin(2 * radianAngle)) / g * howMuchTrajectoryShow;// + (velocity * velocity * Mathf.Cos(2*radianAngle)) / g * howMuchTrajectoryShow;
    //    maxDistance /= 2;
        for (int i = 0; i <= resolution; i++)
        {
            float t = (float)i / (float)resolution;
            arcArray[i] = startPosition + CalculateArcPoint(t, maxDistance);
        }

        return arcArray; 
    }


    private Vector2 CalculateArcPoint(float t, float maxDistance)
    {
        float x = t * maxDistance;
        float sinSign = Mathf.Sin(radianAngle);
        sinSign = sinSign < 0 ? -1 : 1;
       //float y = sinSign * (x * Mathf.Tan(radianAngle) - ((g * x * x) / (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle))));
        float y = (sinSign * (x * Mathf.Tan(radianAngle)) - ((g * x * x) / (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle))));
        //float y = sinSign * (x * Mathf.Tan(radianAngle));
        return new Vector2(x, y);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prediction : MonoBehaviour
{

    public static Vector2[] Plot(Rigidbody2D rigidbody, Vector2 pos, Vector2 velocity, int steps)
    {
        steps *= 100;
        Vector2[] results = new Vector2[steps / 100];

        float timestep = Time.fixedDeltaTime / Physics2D.velocityIterations;// * 100 / 18f * velocity.magnitude;
        Vector2 gravityAccel = Physics2D.gravity * rigidbody.gravityScale * timestep * timestep;
        float drag = 1f - timestep * rigidbody.drag;
        Vector2 moveStep = velocity * timestep;

        for (int i = 0; i < steps; ++i)
        {
            moveStep += gravityAccel;
            moveStep *= drag;
            pos += moveStep;

            if (i % 100 == 0)
                results[i/100] = pos;
        }

        return results;
    }
}

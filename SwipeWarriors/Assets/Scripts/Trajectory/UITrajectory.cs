using UnityEngine;

public class UITrajectory: MonoBehaviour
{
    private LineRenderer lr;
    public float velocity;
    public float angle;
    public int resolution = 10;

    private float g;
    private float radianAngle;

    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
        g = Mathf.Abs(Physics2D.gravity.y);
    }

    void OnValidate()
    {
        if(lr != null)
        {
            RenderArc();
        }
    }

    void Start()
    {
        RenderArc();
    }

    private void RenderArc()
    {
        lr.SetVertexCount(resolution + 1);
        lr.SetPositions(CalculationsArray());
    }

    private Vector3[] CalculationsArray()
    {
        Vector3[] arcArray = new Vector3[resolution + 1];
        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = (velocity * velocity * Mathf.Sin(2 * radianAngle)) / g;

        for(int i = 0; i <= resolution; i++)
        {
            float t = (float)i / (float)resolution;
            arcArray[i] = CalculateArcPoint(t, maxDistance);
        }

        return arcArray;
    }

    private Vector3 CalculateArcPoint(float t, float maxDistance)
    {
        float x = t * maxDistance;
        float y = x * Mathf.Tan(radianAngle) - ((g * x * x) / (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));

        return new Vector3(x, y);
    }
}

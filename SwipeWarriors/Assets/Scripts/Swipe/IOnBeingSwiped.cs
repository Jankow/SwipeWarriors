﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IOnBeingSwiped
{
    void CharacterSwiped(Vector2 power);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipePivot : MonoBehaviour
{

    [SerializeField] private GameObject swipePivot;
    private GameObject instantiatedSwipePivot;
    [SerializeField] private Canvas UICanvas;


    public void PlaceSwipePivot()
    {
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(UICanvas.transform as RectTransform, Input.mousePosition, UICanvas.worldCamera, out pos);
        Vector2 result = UICanvas.transform.TransformPoint(pos);
        instantiatedSwipePivot = Instantiate(swipePivot, new Vector3(result.x, result.y, 0f), Quaternion.identity, UICanvas.transform);
        RectTransform rect = instantiatedSwipePivot.GetComponent<RectTransform>();
        rect.localPosition = new Vector3(rect.localPosition.x, rect.localPosition.y, 0);
        //instantiatedSwipePivot.transform.position = new Vector3(instantiatedSwipePivot.transform.position.x, instantiatedSwipePivot.transform.position.y, 0f);
    }

    public void DecidePivotColor(float power)
    {
        if (power < 6)
            ColorPivot(Color.red);
        else
            ColorPivot(Color.green);
    }


    private void ColorPivot(Color color)
    {
        var images = instantiatedSwipePivot.GetComponentsInChildren<Image>();
        foreach (var image in images)
        {
            image.color = color;
        }
    }

    public void DestroyPivot()
    {
        Destroy(instantiatedSwipePivot);
    }

}

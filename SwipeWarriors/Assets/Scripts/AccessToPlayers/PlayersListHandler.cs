﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersListHandler : MonoBehaviour, IUpdateMember
{
    [SerializeField] private TeamsManager teamsManager;
    private List<IPlayersListUser> playersListUsers = new List<IPlayersListUser>();
    void IUpdateMember.Clean()
    {
        playersListUsers.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if (mono is IPlayersListUser && !playersListUsers.Contains(mono as IPlayersListUser))
        {
            playersListUsers.Add(mono as IPlayersListUser);
            playersListUsers[playersListUsers.Count - 1].GiveIPlayerList(teamsManager);
        }
    }
}

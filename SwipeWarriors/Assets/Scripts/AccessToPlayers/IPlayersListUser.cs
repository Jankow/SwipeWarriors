﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayersListUser  {
    void GiveIPlayerList(IPlayersList playersList);
}

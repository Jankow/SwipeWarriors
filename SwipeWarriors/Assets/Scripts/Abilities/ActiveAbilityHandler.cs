﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveAbilityHandler : MonoBehaviour, IUpdateMember
{
    [SerializeField] private UIAbility abilityUI;
    private List<IActiveAbilityUser> activeAbilityUsers = new List<IActiveAbilityUser>();

    void IUpdateMember.Clean()
    {
        activeAbilityUsers.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if (mono is IActiveAbilityUser && !activeAbilityUsers.Contains(mono as IActiveAbilityUser))
        {
            activeAbilityUsers.Add(mono as IActiveAbilityUser);
            activeAbilityUsers[activeAbilityUsers.Count - 1].GiveIActiveAbility(abilityUI);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAbility : MonoBehaviour, IActiveAbility
{
    [SerializeField] private Button activateSpellButton;
    private Action onUse;

    void IActiveAbility.AttachActionOnUse(Action onAbilityUse)
    {
        onUse = onAbilityUse;
    }

    void IActiveAbility.DisableSpellUse()
    {
        activateSpellButton.interactable = false;
    }

    void IActiveAbility.EnableSpellUse()
    {
        activateSpellButton.interactable = true;
    }

    private void Start()
    {
        activateSpellButton.onClick.AddListener(() => { onUse(); });
        activateSpellButton.onClick.AddListener(() => { activateSpellButton.interactable = false; });
    }
}

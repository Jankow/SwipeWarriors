﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActiveAbilityUser
{
    void GiveIActiveAbility(IActiveAbility activeAbility);
}

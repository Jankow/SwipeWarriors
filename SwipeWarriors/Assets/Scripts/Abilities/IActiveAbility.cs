﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActiveAbility
{
    void EnableSpellUse();
    void DisableSpellUse();
    void AttachActionOnUse(Action onAbilityUse);
}

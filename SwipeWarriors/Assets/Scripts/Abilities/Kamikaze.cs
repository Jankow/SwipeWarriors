﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kamikaze : MonoBehaviour
{
    private CharacterHealth characterHitting;
    [SerializeField] private Collider2D kamikazeTrigger;
    [SerializeField] private float powerOfExplosion;
    [SerializeField] private int damage;
    private void Awake()
    {
        characterHitting = GetComponent<CharacterHealth>();
        characterHitting.onDie = KamikazeAction;
    }

    private void Start()
    {
        print("Attaching dealDamage by Kamikaze");
        WarriorDealDamage warriorDeal = kamikazeTrigger.GetComponent<WarriorDealDamage>();
        warriorDeal.dealDamage = GetDamage;
        warriorDeal.IsAbleToDamageTurnOwner = true;
    }

    private int GetDamage()
    {
        return damage;
    }

    private void KamikazeAction(Action callback)
    {
        print("kamikaze dying");
        kamikazeTrigger.enabled = true;
        StartCoroutine(WaitTwoFramesThenDie(callback));
    }

    private IEnumerator WaitTwoFramesThenDie(Action callback)
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        Collider2D[] colliders = new Collider2D[30];
        kamikazeTrigger.GetContacts(colliders);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i] != null && colliders[i].tag == "Warrior")
            {
                WarriorsCustomPhysics customPhysics = colliders[i].GetComponent<WarriorsCustomPhysics>();
                if (customPhysics != null)
                {
                    Debug.Log("THrowing opoonntt!!");
                    Vector2 targetPosition = colliders[i].transform.position;
                    Vector2 currentPosition = transform.position;
                    customPhysics.ThrowWarrior(powerOfExplosion * (targetPosition - currentPosition).normalized);
                }
            }
        }
        callback();
    }
}

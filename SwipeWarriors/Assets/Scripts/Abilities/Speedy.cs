﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speedy : MonoBehaviour, IActiveAbilityUser, IOnMyTurnStart, IOnMyTurnEnd
{
    private Rigidbody2D body;
    private IActiveAbility activeAbility;

    private bool isMyTurn;
    private bool isSpellUsed;

    [SerializeField]
    private float speedBoost;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    void IActiveAbilityUser.GiveIActiveAbility(IActiveAbility activeAbility)
    {
        print("Active ability has been given");
        this.activeAbility = activeAbility;
    }

    void IOnMyTurnStart.OnMyTurnStart()
    {
        print("On my turn start for speedy");
        isSpellUsed = false;
        isMyTurn = true;
        activeAbility.AttachActionOnUse(OnAbilityUse);
        StartCoroutine(WatchForEnablingAbility());
    }

    void IOnMyTurnEnd.OnMyTurnEnd()
    {
        print("Speedy turn end");
        isMyTurn = false;
        StopAllCoroutines();
    }

    private IEnumerator WatchForEnablingAbility()
    {
        WaitForFixedUpdate waitForFixed = new WaitForFixedUpdate();
        while (!isSpellUsed)
        {
            if (body.velocity.magnitude > 3f)
            {
                activeAbility.EnableSpellUse();
            }
            if (body.velocity.magnitude < 3f)
            {
                activeAbility.DisableSpellUse();
            }
            yield return waitForFixed;
        }
    }

    private void OnAbilityUse()
    {
        isSpellUsed = true;
        body.velocity += body.velocity.normalized * speedBoost;
    }
}

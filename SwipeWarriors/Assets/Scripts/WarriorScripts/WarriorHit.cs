using System;
using UnityEngine;

public class WarriorHit: MonoBehaviour, IThrowObject, ITurnOrderUser
{
    private Rigidbody2D body;
    private ITurnOrder turnOrder;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }

    private void CalculatePhysicsOnReflectAngle(Vector2 direction, float speed)
    {
        Collider2D[] colliders = new Collider2D[10];
        int points = body.GetContacts(colliders);
        foreach(var col in colliders)
        {
            if(col == null)
                break;

            if(col.tag == "Platform")
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(0, -1), 2f);
                if(hit)
                {
                    PhysicsOnReflectAngle(direction, speed, hit);
                    break;
                }
            }
        }
    }

    private void PhysicsOnReflectAngle(Vector2 direction, float speed, RaycastHit2D hit)
    {
        Vector2 reflectAngle = Vector2.Reflect(direction, hit.normal);
        if(gameObject == null)
            return;

        body.velocity += reflectAngle * speed;// / body.mass;
    }

    void IThrowObject.HitObject(GameObject mainObject, Vector2 direction, float speed)
    {
        if (direction.y > 0)
        {
            if (gameObject == null)
                return;

            Debug.LogWarning("Bugsplat:" + direction + " " + speed );
            body.velocity += direction * speed; //;body.mass;
        }
        else
        {
            CalculatePhysicsOnReflectAngle(direction, speed);
        }
    }
}

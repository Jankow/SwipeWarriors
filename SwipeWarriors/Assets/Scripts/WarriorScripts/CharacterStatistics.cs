﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatistics : MonoBehaviour {

    [SerializeField] protected int health;
    [SerializeField] protected float damageMultiplier;
    [SerializeField] protected float maxSwipeDistance;
    [SerializeField] protected float mass;
}

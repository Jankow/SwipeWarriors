using System.Collections.Generic;
using UnityEngine;

public class OnAirHandler: MonoBehaviour
{
    List<IOnAir> onAirActions = new List<IOnAir>();

    private void Start()
    {
        onAirActions = GetComponentInChildren<IOnAir>().ToList();
    }

    void OnCollisionEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "floor")
        {
            onAirActions.ForEach(x => x.OnFloorAction());
        }
    }

    void OnCollisionExit2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "floor")
        {
            onAirActions.ForEach(x => x.OnAirAction());
        }
    }
}

public interface IOnAir
{
    void OnAirAction();
    void OnFloorAction();
    List<IOnAir> ToList();
}

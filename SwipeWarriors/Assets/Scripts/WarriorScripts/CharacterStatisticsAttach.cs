﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatisticsAttach : CharacterStatistics
{
    private void Start()
    {
        GetComponent<CharacterHealth>().SetStartHealth(health);
        GetComponent<CharacterDamage>().DamageMultiplier = damageMultiplier;
        GetComponent<Rigidbody2D>().mass = mass;
        GetComponent<WarriorSwipeController>().MaxSwipeDistance = maxSwipeDistance;
    }

    [SerializeField]
    private int currentHealth;

    private void Update()
    {
        currentHealth = GetComponent<CharacterHealth>().CurrentHealth;
    }
}

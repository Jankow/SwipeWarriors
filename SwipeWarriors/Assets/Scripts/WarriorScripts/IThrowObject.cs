using UnityEngine;

public interface IThrowObject
{
    void HitObject(GameObject mainObject, Vector2 direction, float speed);
}

using UnityEngine;

public class WarriorSwipeController : MonoBehaviour, ISwipeObject, IOnMyTurnStart, IOnMyTurnEnd, ISwipeUser, IControllingCharacter, ITeamConstraintsUser
{
    private Rigidbody2D body;
    private SpriteRenderer spriteRenderer;
    private Swipe swipe;
    private ITeamConstraints teamConstraints;
    private WarriorsCustomPhysics warriorsCollision;

    public bool IsAbleToControl
    {
        get; set;
    }

    public float MaxSwipeDistance
    {
        get; set;
    }

    private void Awake()
    {
        IsAbleToControl = false;
        warriorsCollision = GetComponentInChildren<WarriorsCustomPhysics>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        body = GetComponent<Rigidbody2D>();
    }

    Transform ISwipeObject.GetTransform()
    {
        if (gameObject != null)
        {
            return transform;
        }
        else
        {
            return null;
        }
    }

    void ISwipeObject.ChangeColor(Color color)
    {
        if (spriteRenderer != null)
            spriteRenderer.color = color;
    }

    bool ISwipeObject.IsAbleToChoose()
    {
        return IsAbleToControl;
    }

    void ISwipeObject.Swipe(Vector2 direction, float speed)
    {
        teamConstraints.DisableCharactersActions();
        Vector2 value = direction * speed;
        body.velocity += value;
        var swipeWanted = body.GetComponentsInChildren<IOnBeingSwiped>();
        foreach(var swipe in swipeWanted)
        {
            swipe.CharacterSwiped(value);
        }
    }


    void IOnMyTurnEnd.OnMyTurnEnd()
    {
        IsAbleToControl = false;
    }

    void IOnMyTurnStart.OnMyTurnStart()
    {
        swipe.SetSwipeObject(this);
        IsAbleToControl = true;
    }

    public void GiveSwipe(Swipe swipe)
    {
        this.swipe = swipe;
    }

    void IControllingCharacter.DisableControlling()
    {
        IsAbleToControl = false;
    }

    void IControllingCharacter.EnableControlling()
    {
        IsAbleToControl = true;
    }

    public void GiveTeamConstraints(ITeamConstraints teamConstraints)
    {
        this.teamConstraints = teamConstraints;
    }
}

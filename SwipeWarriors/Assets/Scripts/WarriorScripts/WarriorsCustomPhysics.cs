using System;
using System.Collections;
using UnityEngine;

public class WarriorsCustomPhysics : MonoBehaviour, IOnBeingSwiped
{
    private Rigidbody2D body;
    private IEnumerator calculateEnemyReboundOnTheEndOfPhysics;
    private Action actionOnEnemy;
    private bool firstFrameAfterShoot;

    void Awake()
    {
        body = transform.parent.GetComponent<Rigidbody2D>();
        StartCoroutine(CalculateEnemyReboundOnTheEndOfPhysics());
    }

    private IEnumerator CalculateEnemyReboundOnTheEndOfPhysics()
    {
        WaitForFixedUpdate waitForFixed = new WaitForFixedUpdate();
        while (true)
        {
            yield return waitForFixed;
            if (actionOnEnemy != null)
            {
                actionOnEnemy();
                actionOnEnemy = null;
            }
        }
    }

    private void WaitForEndOfFrameAndDisableCollisionStay()
    {
        firstFrameAfterShoot = true;
        StartCoroutine(WaitForFrame());
    }

    private IEnumerator WaitForFrame()
    {
        yield return new WaitForFixedUpdate();
        firstFrameAfterShoot = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (firstFrameAfterShoot)
        {
            ExecutePhysics(collision);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        ExecutePhysics(other);
    }

    public void ExecutePhysics(Collider2D other)
    {
        if (other.transform.tag == "Warrior")
        {
            Vector2 result = GetReboundVector(other);
            PhysicsActionOnEnemy(other, result);
            if (!firstFrameAfterShoot)
            {
                PhysicsOnOwnBody(other, result);
            }
        }
    }

    public void ThrowWarrior(Vector2 power)
    {
        body.velocity += power / body.mass;
    }

    private Vector2 GetReboundVector(Collider2D other)
    {
        Vector2 targetPos = other.transform.position;
        Vector2 directionBetweenCollision = targetPos - (Vector2)transform.position;
        directionBetweenCollision.Normalize();
        Vector2 velocityDirection = body.velocity.normalized;
        float cosAngle = GetCosinusBetween2Vectors(velocityDirection, directionBetweenCollision);
        return directionBetweenCollision * body.velocity.magnitude * cosAngle;
    } 

    private void PhysicsOnOwnBody(Collider2D other, Vector2 direction)
    {
        Rigidbody2D enemyBody = other.transform.parent.GetComponent<Rigidbody2D>();
        Vector2 betweenCentres = other.transform.position - body.transform.position;
        Vector2 currentVelocity = body.velocity.normalized;
        float cos = GetCosinusBetween2Vectors(betweenCentres, currentVelocity);
        cos = Mathf.Min(cos, 1 - cos);
        Vector2 reactionVector = body.velocity.Rotate(-cos * 60);
        body.velocity = reactionVector;
    }


    public void PhysicsActionOnEnemy(Collider2D other, Vector2 reboundVector)
    {
        Rigidbody2D enemyBody = other.transform.parent.GetComponent<Rigidbody2D>();
        float physicsCollisionMultiplier = body.mass / (body.mass + enemyBody.mass) * 2f;

        IThrowObject throwObject = other.transform.parent.GetComponent<IThrowObject>();
        actionOnEnemy = () =>
        {   
            throwObject.HitObject(body.gameObject, reboundVector.normalized, reboundVector.magnitude * physicsCollisionMultiplier);
        };
    }

    private float GetCosinusBetween2Vectors(Vector2 a, Vector2 b)
    {
        float result = a.x * b.x +
                       a.y * b.y;

        float lengthA = Mathf.Sqrt(Mathf.Pow(a.x, 2) + Mathf.Pow(a.y, 2));
        float lengthB = Mathf.Sqrt(Mathf.Pow(b.x, 2) + Mathf.Pow(b.y, 2));
        if (lengthA == 0)
            lengthA = 1;
        if (lengthB == 0)
            lengthB = 1;

        result /= lengthA * lengthB;

        return result;
    }

    void IOnBeingSwiped.CharacterSwiped(Vector2 power)
    {
        WaitForEndOfFrameAndDisableCollisionStay();
    }
}

public static class Vector2Extension
{
    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        float radians = degrees * Mathf.Deg2Rad;
        float sin = Mathf.Sin(radians);
        float cos = Mathf.Cos(radians);

        float tx = v.x;
        float ty = v.y;

        return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
    }
}

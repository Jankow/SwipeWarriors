using UnityEngine;

public class KillOnTouch: MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Transform obj = collision.transform;
        do
        {
            if(obj.GetComponent<MainObjectSignature>())
            {
                Destroy(obj.gameObject);
                break;
            }

            if(obj.parent == null)
                break;

            obj = obj.parent;
        } while(true);
    }
}

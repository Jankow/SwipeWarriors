﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPointsDisplay : MonoBehaviour
{
    private CharacterHealth characterHealth;
    private ShowingPointsOverCharacter showPoints;

    private void Awake()
    {
        characterHealth = GetComponent<CharacterHealth>();
        showPoints = GetComponentInChildren<ShowingPointsOverCharacter>();
    }

    private void Start()
    {
        characterHealth.onCurrentHealthChange += OnHealthChange;
    }

    private void OnHealthChange(int value)
    {
        if (value == 0)
            return;

        Color textColor;
        if (value < 0)
        {
            textColor = Color.red;
        }
        else
        {
            textColor = Color.green;
        }
        showPoints.ShowPoints(value, textColor);
    }
}

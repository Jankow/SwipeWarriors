﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour, IDealDamage
{
    public int MaxHealth
    {
        get; private set;
    }

    public int CurrentHealth
    {
        get; private set;
    }

    public event Action<int> onCurrentHealthChange;
    public event Action<int> onMaximumHealthChange;
    public Action<Action> onDie;

    public void SetStartHealth(int healthValue)
    {
        CurrentHealth = healthValue;
        MaxHealth = healthValue;
        InvokeHealthChanges();
    }

    private void InvokeHealthChanges()
    {
        if (onCurrentHealthChange != null)
            onCurrentHealthChange(CurrentHealth);
        if (onMaximumHealthChange != null)
            onMaximumHealthChange(MaxHealth);
    }

    internal void AddHealth(int healingValue)
    {
        if (onCurrentHealthChange != null)
            onCurrentHealthChange(healingValue);

        CurrentHealth += healingValue;
        CurrentHealth = Math.Min(CurrentHealth, MaxHealth);
    }
   

    void IDealDamage.Damage(int damage)
    {
        if (onCurrentHealthChange != null)
            onCurrentHealthChange(-damage);

        CurrentHealth -= damage;
        CurrentHealth = Mathf.Max(CurrentHealth, 0);
        if (CurrentHealth == 0)
            Die();
    }

    private void Die()
    {
        // todo: to ponizej ma zadzialac, propozycja - przeniesc do oddzielnego skryptu
        //(GetComponent<NormalMan>() as IOnDestroy).NotifyOnDestroy();
        if (onDie == null)
        {
            // Tutaj wstaw jakas animacje
            Destroy(gameObject);
        }
        else
        {
            Action<Action> onDieTemp = onDie;
            onDie = null;
            onDieTemp(Die);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthSliderDisplay : MonoBehaviour, IOnMyTeamTurnStart, IOnEnemyTurnStart
{
    [SerializeField]
    private CharacterHealth characterHealth;
    [SerializeField]
    private Image healthSlider;

    void IOnEnemyTurnStart.OnEnemyTurnStart()
    {
        if (healthSlider != null)
            healthSlider.color = Color.red;
    }
     
    void IOnMyTeamTurnStart.OnMyTeamTurnStart()
    {
        if (healthSlider != null)
            healthSlider.color = Color.green;
    }

    // Update is called once per frame
    void Update()
    {
        healthSlider.fillAmount = (float)characterHealth.CurrentHealth / (float)characterHealth.MaxHealth;
    }
}

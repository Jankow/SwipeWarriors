﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundaries
{
    private float minX, minY, maxX, maxY;
    public Vector2 MinBoundaries { get { return new Vector2(minX, minY); } set { minX = value.x; minY = value.y; } }
    public Vector2 MaxBoundaries { get { return new Vector2(maxX, maxY); } set { maxX = value.x; maxY = value.y; } }
    public Vector2 CenterOfBoundaries { get { return (MaxBoundaries + MinBoundaries) / 2; } }
    public float BoundariesRange { get { return Mathf.Max(maxX - minX, maxY - minY); } }
    public void TryToChangeBounds(Vector2 vector2)
    {
        if (vector2.x < minX)
            minX = vector2.x;
        if (vector2.y < minY)
            minY = vector2.y;

        if (vector2.x > maxX)
            maxX = vector2.x;
        if (vector2.y > maxY)
            maxY = vector2.y;
    }
    public void ResetBounds()
    {
        MinBoundaries = new Vector2(float.MaxValue, float.MaxValue);
        MaxBoundaries = new Vector2(float.MinValue, float.MinValue);
    }
}
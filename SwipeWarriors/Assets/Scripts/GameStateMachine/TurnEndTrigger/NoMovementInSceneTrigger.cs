﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoMovementInSceneTrigger : MonoBehaviour, ITurnEndTrigger
{
    [SerializeField] private TeamsManager teamsManager;
    private List<Team> playerList = new List<Team>();
    private bool enabled = false;
    private Action action;

    void Awake()
    {
        playerList = teamsManager.GetTeams();
    }

    void Update()
    {
        if (enabled)
        {
            if (CheckTriggerCondition())
                action();
        }
    }

    void IStateTrigger.EnableTrigger()
    {
        enabled = true;
    }

    void IStateTrigger.DisableTrigger()
    {
        enabled = false;
    }

    void IStateTrigger.GiveActionOnTrigger(Action actionOnTrigger)
    {
        this.action = actionOnTrigger;
    }

    private bool CheckTriggerCondition()
    {
        bool returnValue = true;
        foreach (var team in playerList)
        {
            foreach (var heroCharacter in team.GetCharacters())
            {
                Vector2 vec = heroCharacter.GetComponent<Rigidbody2D>().velocity;
                if (!Mathf.Approximately(vec.y, 0) && !Mathf.Approximately(vec.x, 0))
                    returnValue = false;
            }
        }
        return returnValue;
    }
}

using System.Collections.Generic;
using UnityEngine;

public class BetweenTurnsHandler: MonoBehaviour, IBetweenTurns
{
    private List<IOnBetweenTurns> onBetweenTurns = new List<IOnBetweenTurns>();




    public void Clean()
    {
        onBetweenTurns.Clear();
    }

    public void TryUpdate(MonoBehaviour mono)
    {
        // todo: sprawdzanie jaki to gracz i dodanie do dictionary (a nie do listy)
        if(mono is IOnBetweenTurns)
            onBetweenTurns.Add(mono as IOnBetweenTurns);
    }



    public void BetweenTurns()
    {
        foreach(var item in onBetweenTurns)
        {
            item.OnBetweenTurns();
        }
    }
}

using System.Collections.Generic;
using UnityEngine;

public class EveryTurnStartHandler: MonoBehaviour, IOnTurnStart, IUpdateMember
{
    private List<IOnEveryTurnStart> onEveryTurnStart = new List<IOnEveryTurnStart>();

    void IUpdateMember.Clean()
    {
        onEveryTurnStart.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        // sprawdzanie jaki to gracz i dodanie do dictionary (a nie do listy)
        if(mono is IOnEveryTurnStart)
            onEveryTurnStart.Add(mono as IOnEveryTurnStart);
    }

    void IOnTurnStart.OnTurnStart()
    {
        foreach(var item in onEveryTurnStart)
        {
            item.OnEveryTurnStart();
        }
    }
}

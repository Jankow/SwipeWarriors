using System.Collections.Generic;
using UnityEngine;

public class MyTurnStartHandler: MonoBehaviour, IOnTurnStart, IUpdateMember, ITurnOrderUser
{
    private Dictionary<IOnMyTurnStart, Character> onMyTurnStart = new Dictionary<IOnMyTurnStart, Character>();
    private ITurnOrder turnOrder;// todo: DAJ REFERENCJE KOBIETO

    void IOnTurnStart.OnTurnStart()
    {
        Debug.Log("MyTurnStartHandler");
        foreach (var item in onMyTurnStart)
        {
            if(item.Value == turnOrder.GetCurrentCharacter())
                if(item.Key != null)
                    item.Key.OnMyTurnStart();
        }
    }

    void IUpdateMember.Clean()
    {
        onMyTurnStart.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IOnMyTurnStart)
        {
            RemoveSingleMono(mono);
            onMyTurnStart.Add(mono as IOnMyTurnStart, mono.GetComponent<Character>());
        }
    }

    private void RemoveSingleMono(MonoBehaviour mono)
    {
        if(onMyTurnStart.ContainsKey(mono as IOnMyTurnStart))
            onMyTurnStart.Remove(mono as IOnMyTurnStart);
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }
}

using System.Collections.Generic;
using UnityEngine;

public class EnemyTeamTurnStartHandler: MonoBehaviour, IOnTurnStart, IUpdateMember, ITurnOrderUser
{
    private Dictionary<IOnEnemyTurnStart, ITeamID> onEnemyTurnStart = new Dictionary<IOnEnemyTurnStart, ITeamID>();
    private ITurnOrder turnOrder;// todo: DAJ REFERENCJE KOBIETO

    void IUpdateMember.Clean()
    {
        onEnemyTurnStart.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IOnEnemyTurnStart)
        {
            RemoveSingleMono(mono);
            onEnemyTurnStart.Add(mono as IOnEnemyTurnStart, mono.GetComponentInParent<ITeamID>());
        }
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }

    private void RemoveSingleMono(MonoBehaviour mono)
    {
        if(onEnemyTurnStart.ContainsKey(mono as IOnEnemyTurnStart))
            onEnemyTurnStart.Remove(mono as IOnEnemyTurnStart);
    }

    void IOnTurnStart.OnTurnStart()
    {
        foreach(var item in onEnemyTurnStart)
        {
            if(item.Value.TeamID != turnOrder.GetCurrentTeam().TeamID)
                item.Key.OnEnemyTurnStart();
        }
    }
}

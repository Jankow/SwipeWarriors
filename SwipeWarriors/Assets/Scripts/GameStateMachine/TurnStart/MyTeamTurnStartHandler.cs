﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTeamTurnStartHandler : MonoBehaviour, IOnTurnStart, IUpdateMember, ITurnOrderUser
{
    private Dictionary<IOnMyTeamTurnStart, ITeamID> onTeamTurnStart = new Dictionary<IOnMyTeamTurnStart, ITeamID>();
    private ITurnOrder turnOrder;// todo: DAJ REFERENCJE KOBIETO

    void IOnTurnStart.OnTurnStart()
    {
        Debug.Log("My team turn start handler");
        foreach(var item in onTeamTurnStart)
        {
            if (item.Value.TeamID == turnOrder.GetCurrentTeam().TeamID)
                if(item.Key != null)
                    item.Key.OnMyTeamTurnStart();
        }
    }

    void IUpdateMember.Clean()
    {
        onTeamTurnStart.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IOnMyTeamTurnStart)
        {
            RemoveSingleMono(mono);
            onTeamTurnStart.Add(mono as IOnMyTeamTurnStart, mono.GetComponentInParent<ITeamID>());
        }
    }

    private void RemoveSingleMono(MonoBehaviour mono)
    {
        if(onTeamTurnStart.ContainsKey(mono as IOnMyTeamTurnStart))
            onTeamTurnStart.Remove(mono as IOnMyTeamTurnStart);
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }
}

using System.Collections.Generic;
using UnityEngine;

public class TurnStartHandler: MonoBehaviour, ITurnStart
{
    private List<IOnTurnStart> onTurnStart = new List<IOnTurnStart>();

    void ITurnStart.TurnStart()
    {
        foreach(var item in onTurnStart)
        {
            item.OnTurnStart();
        }
    }

    void IUpdateMember.Clean()
    {
        onTurnStart.Clear();
    }


    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        // sprawdzanie jaki to gracz i dodanie do dictionary (a nie do listy)
        if(mono is IOnTurnStart)
            onTurnStart.Add(mono as IOnTurnStart);
    }
}

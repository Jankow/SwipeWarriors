public interface ITurnStart : IUpdateMember
{
    void TurnStart();
}

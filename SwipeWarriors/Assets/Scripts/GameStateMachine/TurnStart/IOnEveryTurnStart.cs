﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public interface IOnEveryTurnStart
{
    void OnEveryTurnStart();
}

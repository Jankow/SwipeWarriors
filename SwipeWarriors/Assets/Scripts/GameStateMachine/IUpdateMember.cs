using UnityEngine;

public interface IUpdateMember
{
    void Clean();
    void TryUpdate(MonoBehaviour mono);
}

using System;

public interface IStateTrigger
{
    void EnableTrigger();
    void DisableTrigger();
    void GiveActionOnTrigger(Action actionOnTrigger);
}

using UnityEngine;

public partial class GameStateMachine: MonoBehaviour
{
    public void InjectGameStartTrigger(IGameStartTrigger gameStartTrigger)
    {
        this.gameStartTrigger = gameStartTrigger;
        gameStartTrigger.GiveActionOnTrigger(() =>
            {
                gameStartTrigger.DisableTrigger();
                gameStart.GameStart();
                betweenTurnsTrigger.EnableTrigger();
                gameEndTrigger.EnableTrigger();
            });
    }

    public void InjectGameStart(IGameStart gameStart)
    {
        this.gameStart = gameStart;
    }

    public void InjectTurnStartTrigger(ITurnStartTrigger turnStartTrigger)
    {
        this.turnStartTrigger = turnStartTrigger;
        turnStartTrigger.GiveActionOnTrigger(() =>
        {
            turnStartTrigger.DisableTrigger();
            turnStart.TurnStart();
            turnEndTrigger.EnableTrigger();
        });
    }

    public void InjectTurnStart(ITurnStart turnStart)
    {
        this.turnStart = turnStart;
    }

    public void InjectBetweenTurnsTrigger(IBetweenTurnsTrigger betweenTurnsTrigger)
    {
        this.betweenTurnsTrigger = betweenTurnsTrigger;
        betweenTurnsTrigger.GiveActionOnTrigger(() =>
        {
            betweenTurnsTrigger.DisableTrigger();
            betweenTurns.BetweenTurns();
            turnStartTrigger.EnableTrigger();
        });
    }

    public void InjectBetweenTurns(IBetweenTurns betweenTurns)
    {
        this.betweenTurns = betweenTurns;
    }

    public void InjectTurnEndTrigger(ITurnEndTrigger turnEndTrigger)
    {
        this.turnEndTrigger = turnEndTrigger;
        turnEndTrigger.GiveActionOnTrigger(() =>
        {
            turnEndTrigger.DisableTrigger();
            turnEnd.TurnEnd();
            betweenTurnsTrigger.EnableTrigger();
        });
    }

    public void InjectTurnEnd(ITurnEnd turnEnd)
    {
        this.turnEnd = turnEnd;
    }

    public void InjectGameEndTrigger(IGameEndTrigger gameEndTrigger)
    {
        this.gameEndTrigger = gameEndTrigger;
        gameEndTrigger.GiveActionOnTrigger(() =>
        {
            Debug.Log("Game ending!");
            gameEndTrigger.DisableTrigger();
            gameEnd.GameEnd();
        });
    }

    public void InjectGameEnd(IGameEnd gameEnd)
    {
        this.gameEnd = gameEnd;
    }
}

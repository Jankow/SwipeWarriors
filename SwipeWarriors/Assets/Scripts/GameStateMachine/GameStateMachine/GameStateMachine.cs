using UnityEngine;

public partial class GameStateMachine: MonoBehaviour
{
    void Start()
    {
        this.gameStartTrigger.EnableTrigger();
    }


    private IGameStartTrigger gameStartTrigger;
    private IGameStart gameStart;

    private ITurnStartTrigger turnStartTrigger;
    private ITurnStart turnStart;

    private ITurnEndTrigger turnEndTrigger;
    private ITurnEnd turnEnd;

    private IBetweenTurnsTrigger betweenTurnsTrigger;
    private IBetweenTurns betweenTurns;

    private IGameEndTrigger gameEndTrigger;
    private IGameEnd gameEnd;




}

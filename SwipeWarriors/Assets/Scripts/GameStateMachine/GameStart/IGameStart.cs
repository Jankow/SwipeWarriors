﻿public interface IGameStart : IUpdateMember
{
    void GameStart();
}

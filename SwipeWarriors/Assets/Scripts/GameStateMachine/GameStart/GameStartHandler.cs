﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartHandler : MonoBehaviour, IGameStart
{
    private List<IOnGameStart> onGameStart = new List<IOnGameStart>();

    void IGameStart.GameStart()
    {
        foreach (var item in onGameStart)
        {
            item.OnGameStart();
        }
    }

    void IUpdateMember.Clean()
    {
        onGameStart.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        // todo: sprawdzanie jaki to gracz i dodanie do dictionary (a nie do listy)
        if (mono is IOnGameStart)
            onGameStart.Add(mono as IOnGameStart);
    }
}

using System.Collections.Generic;
using UnityEngine;

public class TurnOrderHandler: MonoBehaviour, IUpdateMember
{
    private List<ITurnOrderUser> turnOrderUsers = new List<ITurnOrderUser>();
    private ITurnOrder playersTurnOrder;

    public void InjectPlayersTurnOrder(ITurnOrder turnOrder)
    {
        playersTurnOrder = turnOrder;
    }

    void IUpdateMember.Clean()
    {
        turnOrderUsers.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        // sprawdzanie jaki to gracz i dodanie do dictionary (a nie do listy)
        if(mono is ITurnOrderUser)
        {
            turnOrderUsers.Add(mono as ITurnOrderUser);
            //      print("PrintOrderHandler.ListOfUsers index: " + (turnOrderUsers.Count - 1));
            turnOrderUsers[turnOrderUsers.Count - 1].GiveITurnOrder(playersTurnOrder);
        }

    }
}

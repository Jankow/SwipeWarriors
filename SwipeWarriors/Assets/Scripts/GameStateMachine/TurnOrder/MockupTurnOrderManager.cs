using UnityEngine;

public class MockupTurnOrderManager: MonoBehaviour
{
    [SerializeField] private PlayerQueue currentTurnOrder;
    [SerializeField] private TurnOrderHandler turnOrderHandler;

    private void Awake()
    {
        turnOrderHandler.InjectPlayersTurnOrder(currentTurnOrder);
    }
}

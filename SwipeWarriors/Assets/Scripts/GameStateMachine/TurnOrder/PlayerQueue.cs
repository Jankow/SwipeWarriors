using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerQueue : MonoBehaviour, ITurnOrder, IOnBetweenTurns
{
    [SerializeField] private TeamsManager teamsManager;
    [SerializeField] private Text turnChangerUi;
    private List<Team> teamsList = new List<Team>();
    private int currentTeam = -1;

    public void OnBetweenTurns()
    {
        currentTeam = PlayerInAdvance(1);
        turnChangerUi.text = teamsList[currentTeam].TeamID + "'s turn!";
    }

    void Start()
    {
        teamsList = teamsManager.GetTeams();
    }

    private int PlayerInAdvance(int playersInAdvance)
    {
        if (teamsList.Count > 0)
            return (currentTeam + playersInAdvance) % teamsList.Count;
        else
            return 0;
    }

    Team ITurnOrder.GetCurrentTeam()
    {
        return teamsList[PlayerInAdvance(0)];
    }

    Team ITurnOrder.GetTeamInAdvance(int playersInAdvance)
    {
        return teamsList[PlayerInAdvance(playersInAdvance)];
    }

    Character ITurnOrder.GetCurrentCharacter()
    {
        return (this as ITurnOrder).GetCurrentTeam().CurrentPlayingCharacter;
    }
}

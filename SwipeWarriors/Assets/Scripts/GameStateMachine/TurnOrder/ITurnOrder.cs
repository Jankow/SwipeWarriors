public interface ITurnOrder
{
    Team GetCurrentTeam();
    Team GetTeamInAdvance(int playersInAdvance);
    Character GetCurrentCharacter();
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndTrigger : MonoBehaviour, IGameEndTrigger
{
    [SerializeField] private TeamsManager teamsManager;
    private List<Team> playerList = new List<Team>();
    private bool enabled = false;
    private Action action;

    void Start()
    {
        playerList = teamsManager.GetTeams();
    }

    void Update()
    {
        if (enabled)
        {
            if (CheckTriggerCondition())
                action();
        }
    }

    void IStateTrigger.EnableTrigger()
    {
        enabled = true;
    }

    void IStateTrigger.DisableTrigger()
    {
        enabled = false;
    }

    void IStateTrigger.GiveActionOnTrigger(Action actionOnTrigger)
    {
        this.action = actionOnTrigger;
    }

    private bool CheckTriggerCondition()
    {
        bool returnValue = true;
        int teamsAlive = 0;
        foreach (var team in playerList)
        {
            bool teamAlive = false;
            foreach (var heroCharacter in team.GetCharacters())
            {
                if (heroCharacter != null)
                    teamAlive = true;
            }
            if (teamAlive)
                teamsAlive++;
        }
        returnValue = teamsAlive <= 1;
        
        return returnValue;
    }
}

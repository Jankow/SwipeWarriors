﻿public interface IGameEnd : IUpdateMember
{
    void GameEnd();
}

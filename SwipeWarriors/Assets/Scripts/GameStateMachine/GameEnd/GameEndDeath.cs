﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndDeath : MonoBehaviour, IOnGameEnd
{
    public void OnGameEnd()
    {
        SceneManager.LoadScene("MainMenu");
    }
}

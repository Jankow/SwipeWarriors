using System.Collections.Generic;
using UnityEngine;

public class GameEndHandler: MonoBehaviour, IGameEnd
{
    private List<IOnGameEnd> onGameEnd = new List<IOnGameEnd>();

    public void GameEnd()
    {
        foreach(var item in onGameEnd)
        {
            item.OnGameEnd();
        }
    }

    public void Clean()
    {
        onGameEnd.Clear();
    }

    public void TryUpdate(MonoBehaviour mono)
    {
        // todo: sprawdzanie jaki to gracz i dodanie do dictionary (a nie do listy)
        if(mono is IOnGameEnd)
            onGameEnd.Add(mono as IOnGameEnd);
    }
}

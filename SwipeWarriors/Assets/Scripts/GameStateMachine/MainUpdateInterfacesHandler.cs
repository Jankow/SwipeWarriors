using System.Collections.Generic;
using UnityEngine;

public class MainUpdateInterfacesHandler: MonoBehaviour
{
    private List<IUpdateMember> interfacesHandlers = new List<IUpdateMember>();

    public void UpdateAllInterfaces()
    {
        interfacesHandlers.Clear();
        MonoBehaviour[] monoBehaviours = FindObjectsOfType<MonoBehaviour>();
        FindAllInterfacesHandlers(monoBehaviours);
        ClearAllHandlers();
        UpdateAllHandlers(monoBehaviours);
    }

    public void UpdateSingleMono(MonoBehaviour mono)
    {
        UpdateMono(mono);
    }

    private void Start()
    {
        UpdateAllInterfaces();
    }

    private void FindAllInterfacesHandlers(MonoBehaviour[] monoBehaviours)
    {
        foreach(var mono in monoBehaviours)
        {
            if(mono is IUpdateMember)
            {
                interfacesHandlers.Add(mono as IUpdateMember);
            }
        }
    }

    private void ClearAllHandlers()
    {
        foreach (var interfacesHandler in interfacesHandlers)
        {
            interfacesHandler.Clean();
        }
    }

    private void UpdateAllHandlers(MonoBehaviour[] monoBehaviours)
    {
        foreach(var mono in monoBehaviours)
        {
            UpdateMono(mono);
        }
    }

    private void UpdateMono(MonoBehaviour mono)
    {
        foreach (var interfacesHandler in interfacesHandlers)
        {
            interfacesHandler.TryUpdate(mono);
        }
    }
}

using System.Collections.Generic;
using UnityEngine;

public class EnemyTeamTurnEndHandler: MonoBehaviour, IOnTurnEnd, IUpdateMember, ITurnOrderUser
{
    Dictionary<IOnEnemyTurnEnd, ITeamID> onEveryTurnEnd = new Dictionary<IOnEnemyTurnEnd, ITeamID>();

    private ITurnOrder turnOrder; // todo: Klasę rozdającą listę kolejnych graczy, wtedy tutaj trzeba pobrać czyja tura się skończyła - przemyśleć i zaprojektować

    void IOnTurnEnd.OnTurnEnd()
    {
        foreach(var item in onEveryTurnEnd)
        {
            if(item.Value.TeamID != turnOrder.GetCurrentTeam().TeamID)
                item.Key.OnEnemyTurnEnd();
        }
    }

    void IUpdateMember.Clean()
    {
        onEveryTurnEnd.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IOnEnemyTurnEnd)
        {
            RemoveSingleMono(mono);
            onEveryTurnEnd.Add(mono as IOnEnemyTurnEnd, mono.GetComponent<ITeamID>());
        }
    }

    private void RemoveSingleMono(MonoBehaviour mono)
    {
        if(onEveryTurnEnd.ContainsKey(mono as IOnEnemyTurnEnd))
            onEveryTurnEnd.Remove(mono as IOnEnemyTurnEnd);
    }

    public void GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }
}

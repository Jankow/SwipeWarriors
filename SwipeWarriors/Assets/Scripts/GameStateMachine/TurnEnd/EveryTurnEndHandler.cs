using System.Collections.Generic;
using UnityEngine;

public class EveryTurnEndHandler: MonoBehaviour, IOnTurnEnd, IUpdateMember
{
    List<IOnEveryTurnEnd> onEveryTurnEndHandler = new List<IOnEveryTurnEnd>();

    void IOnTurnEnd.OnTurnEnd()
    {
        foreach(var item in onEveryTurnEndHandler)
        {
            item.OnEveryTurnEnd();
        }
    }

    void IUpdateMember.Clean()
    {
        onEveryTurnEndHandler.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IOnEveryTurnEnd)
        {
            RemoveSingleMono(mono);
            onEveryTurnEndHandler.Add(mono as IOnEveryTurnEnd);
        }
    }

    private void RemoveSingleMono(MonoBehaviour mono)
    {
        if(onEveryTurnEndHandler.Contains(mono as IOnEveryTurnEnd))
            onEveryTurnEndHandler.Remove(mono as IOnEveryTurnEnd);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnEndHandler : MonoBehaviour, ITurnEnd
{
    //private Dictionary<PlayerInfo, IOnTurnEnd> onTurnEnd;
    private List<IOnTurnEnd> onTurnEnd = new List<IOnTurnEnd>();

    void ITurnEnd.TurnEnd()
    {
        foreach (var item in onTurnEnd)
        {
            item.OnTurnEnd();
        }
    }
    
    void IUpdateMember.Clean()
    {
        onTurnEnd.Clear();
    }


    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        // sprawdzanie jaki to gracz i dodanie do dictionary (a nie do listy)
        if (mono is IOnTurnEnd)
            onTurnEnd.Add(mono as IOnTurnEnd);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTeamTurnEndHandler : MonoBehaviour, IUpdateMember, IOnTurnEnd, ITurnOrderUser
{
    private Dictionary<IOnMyTeamTurnEnd, ITeamID> onMyTeamTurnEnd = new Dictionary<IOnMyTeamTurnEnd, ITeamID>();
    private ITurnOrder turnOrder;

    void IOnTurnEnd.OnTurnEnd()
    {
        foreach(var item in onMyTeamTurnEnd)
        {
            if(item.Value.TeamID == turnOrder.GetCurrentTeam().TeamID)
                item.Key.OnMyTeamTurnEnd();
        }
    }

    void IUpdateMember.Clean()
    {
        onMyTeamTurnEnd.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IOnMyTeamTurnEnd)
        {
            RemoveSingleMono(mono);
            onMyTeamTurnEnd.Add(mono as IOnMyTeamTurnEnd, mono.GetComponent<ITeamID>());
        }
    }

    private void RemoveSingleMono(MonoBehaviour mono)
    {
        if(onMyTeamTurnEnd.ContainsKey(mono as IOnMyTeamTurnEnd))
            onMyTeamTurnEnd.Remove(mono as IOnMyTeamTurnEnd);
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }
}

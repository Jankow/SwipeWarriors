using System.Collections.Generic;
using UnityEngine;

public class MyTurnEndHandler: MonoBehaviour, IOnTurnEnd, IUpdateMember, ITurnOrderUser
{
    private Dictionary<IOnMyTurnEnd, Character> onMyTurnEnd = new Dictionary<IOnMyTurnEnd, Character>();
    private ITurnOrder turnOrder;

    void IOnTurnEnd.OnTurnEnd()
    {
        print("MyTurnEnd Handler");
        foreach(var item in onMyTurnEnd)
        {
            if(item.Value == turnOrder.GetCurrentCharacter())
                item.Key.OnMyTurnEnd();
        }
    }

    void IUpdateMember.Clean()
    {
        onMyTurnEnd.Clear();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IOnMyTurnEnd)
        {
            RemoveSingleMono(mono);
            onMyTurnEnd.Add(mono as IOnMyTurnEnd, mono.GetComponent<Character>());
        }
    }

    private void RemoveSingleMono(MonoBehaviour mono)
    {
        if(onMyTurnEnd.ContainsKey(mono as IOnMyTurnEnd))
            onMyTurnEnd.Remove(mono as IOnMyTurnEnd);
    }

    void ITurnOrderUser.GiveITurnOrder(ITurnOrder turnOrder)
    {
        this.turnOrder = turnOrder;
    }
}

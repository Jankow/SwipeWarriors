﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IOnMyTeamTurnEnd {
    void OnMyTeamTurnEnd();
}

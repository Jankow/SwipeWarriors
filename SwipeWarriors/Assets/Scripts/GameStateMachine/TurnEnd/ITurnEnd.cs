﻿public interface ITurnEnd : IUpdateMember
{
    void TurnEnd();
}

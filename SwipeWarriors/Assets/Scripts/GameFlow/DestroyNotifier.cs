﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyNotifier : MonoBehaviour
{
    [SerializeField] private TeamsManager teamsManager;

    public void Notify(IOnDestroy onDestroy)
    {
        if (onDestroy is Character)
        {
            teamsManager.RemoveCharacter(onDestroy as Character);
        }
    }
}

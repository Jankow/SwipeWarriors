using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController: MonoBehaviour
{
    private void EndGame()
    {
        // todo: shows wind popup with prizes and then load scene with statistics

        SceneManager.LoadScene("GameCreator");
    }
}

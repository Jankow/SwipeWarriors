using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TurnChangerUI: MonoBehaviour
{
    [SerializeField] private Text whichPlayerTurn;

    void Start()
    {

    }

    void Update()
    {

    }

    public void ShowText(string textToShow)
    {
        whichPlayerTurn.text = textToShow;
        StartCoroutine(HideText(4));
    }

    private IEnumerator HideText(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        whichPlayerTurn.text = "";
    }
}

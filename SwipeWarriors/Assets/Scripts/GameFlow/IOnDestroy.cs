﻿public interface IOnDestroy
{
    void NotifyOnDestroy();
}

using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerWarriors
{
    private PlayerInfo playerInfo;

    public PlayerInfo PlayerInfo
    {
        get
        {
            return playerInfo;
        }
        set
        {
            playerInfo = value;
        }
    }


    [SerializeField]
    private List<WarriorSwipeController> warriors; // Lista z wojownikami konkretnego playera

    public List<WarriorSwipeController> Warriors
    {
        get
        {
            return warriors;
        }
        set
        {
            warriors = value;
        }
    }
}
using UnityEngine;

public class TriggersToGameStateMachineManager: MonoBehaviour, IUpdateMember
{
    private GameStateMachine gameStateMachine;
    private void Awake()
    {
        gameStateMachine = GetComponent<GameStateMachine>();
    }

    void IUpdateMember.Clean()
    {
        // throw new System.NotImplementedException();
    }

    void IUpdateMember.TryUpdate(MonoBehaviour mono)
    {
        if(mono is IStateTrigger)
        {
            if(mono is IGameStartTrigger)
            {
                Debug.Log("Injecting GameStart Trigger");
                gameStateMachine.InjectGameStartTrigger(mono as IGameStartTrigger);
            }
            else if (mono is IGameEndTrigger)
            {
                Debug.Log("Injecting TurnEnd Trigger");
                gameStateMachine.InjectGameEndTrigger(mono as IGameEndTrigger);
            }
            else if(mono is IBetweenTurnsTrigger)
            {
                Debug.Log("Injecting BetweenTurns Trigger");
                gameStateMachine.InjectBetweenTurnsTrigger(mono as IBetweenTurnsTrigger);
            }
            else if(mono is ITurnStartTrigger)
            {
                Debug.Log("Injecting TurnStart Trigger");
                gameStateMachine.InjectTurnStartTrigger(mono as ITurnStartTrigger);
            }
            else if(mono is ITurnEndTrigger)
            {
                Debug.Log("Injecting TurnEnd Trigger");
                gameStateMachine.InjectTurnEndTrigger(mono as ITurnEndTrigger);
            }
        }
        if(mono is IUpdateMember)
        {
            if(mono is IGameStart)
            {
                Debug.Log("Injecting GameStart Handler");
                gameStateMachine.InjectGameStart(mono as IGameStart);
            }
            else if(mono is IGameEnd)
            {
                Debug.Log("Injecting GameEnd Handler");
                gameStateMachine.InjectGameEnd(mono as IGameEnd);
            }
            else if(mono is ITurnStart)
            {
                Debug.Log("Injecting TurnStart Handler");
                gameStateMachine.InjectTurnStart(mono as ITurnStart);
            }
            else if(mono is ITurnEnd)
            {
                Debug.Log("Injecting TurnEnd Handler");
                gameStateMachine.InjectTurnEnd(mono as ITurnEnd);
            }
            else if(mono is IBetweenTurns)
            {
                Debug.Log("Injecting BetweenTurns Handler");
                gameStateMachine.InjectBetweenTurns(mono as IBetweenTurns);
            }
        }
    }
}

using System;
using System.Collections.Generic;
using UnityEngine;

public class TurnChanger: MonoBehaviour, ITurnEndTrigger
{
    [SerializeField]
    private TeamsManager teamsManager;
    private List<Team> teamsList = new List<Team>();




    private bool isOn = false;
    private Action actionOnTrigger;
    private bool anyOneMoved = false;
    private int countNoMovementFrames = 0;



    public event Action<PlayerInfo> OnTurnEnd;
    public event Action<PlayerInfo> OnTurnStart;

    //public void SetListOfPlayers(List<PlayerInfo> list)
    //{
    //  listOfPlayers = list;
    //}

    void Start()
    {
        teamsList = teamsManager.GetTeams();
    }

    void Update()
    {
        if(isOn)
        {
            if(IfSomeoneIsMoving())
            {
                anyOneMoved = true;
            }
            else
            {
                //     countNoMovementFrames++;
                if(anyOneMoved)
                {
                    Debug.Log("Koniec tury");
                    ChangeTurn();
                    actionOnTrigger();
                }
            }
        }
    }

    public void ChangeTurn()
    {
        //if (OnTurnEnd != null)
        //    OnTurnEnd(listOfPlayers[whoseTurn]);
        Debug.Log("TurnChanger.ChangingTurn");

        //if (OnTurnStart != null)
        //    OnTurnStart(listOfPlayers[whoseTurn]);
    }

    void IStateTrigger.EnableTrigger()
    {
        Debug.Log("Koniec tury trigger enabled");
        isOn = true;
        anyOneMoved = false;


    }

    void IStateTrigger.DisableTrigger()
    {
        isOn = false;
    }

    void IStateTrigger.GiveActionOnTrigger(Action actionOnTrigger)
    {
        this.actionOnTrigger = actionOnTrigger;
    }

    /// <summary>
    /// Return true when no character is moving, returns false otherwise.
    /// </summary>
    /// <returns></returns>
    private bool IfSomeoneIsMoving()
    {
        bool anyOneIsMoving = false;
        foreach(var team in teamsList)
        {
            foreach(Character character in team.GetCharacters())
            {
                //  if(!character.GetComponent<Rigidbody2D>().IsSleeping())
                //     anyOneMoved = false;

                //Vector2 vec = character.GetComponent<Rigidbody2D>().velocity;
                if(character.GetComponent<Rigidbody2D>() != null && !character.GetComponent<Rigidbody2D>().IsSleeping())//!Mathf.Approximately(vec.y, 0) && !Mathf.Approximately(vec.x, 0))
                    anyOneIsMoving = true;
            }
        }
        //if(anyOneIsMoving == false)
        //    Debug.Log("No movement detected.");
        return anyOneIsMoving;
    }
}

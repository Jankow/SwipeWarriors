using System;
using UnityEngine;

public class TurnStartTrigger: MonoBehaviour, ITurnStartTrigger
{
    private Action actionOnTrigger;

    void IStateTrigger.EnableTrigger()
    {
        Debug.Log("Enabling TurnStart Trigger");
        if (actionOnTrigger != null)
            actionOnTrigger();
        else
            Debug.Log("No action on trigger between turns");
    }

    void IStateTrigger.DisableTrigger()
    {
        Debug.Log("Disabling TurnStart Trigger");
        // Nie ma po co wylaczac na mock'upie
    }

    void IStateTrigger.GiveActionOnTrigger(Action actionOnTrigger)
    {
        this.actionOnTrigger = actionOnTrigger;
    }
}


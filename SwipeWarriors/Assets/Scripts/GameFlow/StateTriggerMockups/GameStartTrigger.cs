using System;
using System.Collections;
using UnityEngine;

public class GameStartTrigger: MonoBehaviour, IGameStartTrigger
{
    private Action actionOnTrigger;

    void IStateTrigger.EnableTrigger()
    {
        Debug.Log("Enabling GameStart Trigger");
        if(actionOnTrigger != null)
            StartCoroutine(StartGameInSecond());
        else
            Debug.Log("No action on trigger between turns");
    }

    private IEnumerator StartGameInSecond()
    {
        yield return new WaitForSeconds(3f);
        actionOnTrigger();
    }

    void IStateTrigger.DisableTrigger()
    {
        Debug.Log("Disabling GameStart Trigger");
        // Nie ma po co wylaczac na mock'upie
    }

    void IStateTrigger.GiveActionOnTrigger(Action actionOnTrigger)
    {
        this.actionOnTrigger = actionOnTrigger;
    }
}
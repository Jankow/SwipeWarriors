using System;
using UnityEngine;

public class BetweenTurnsTrigger: MonoBehaviour, IBetweenTurnsTrigger
{

    private Action actionOnTrigger;
    void IStateTrigger.EnableTrigger()
    {
        Debug.Log("Enabling BetweenTurns Trigger");
        if(actionOnTrigger != null)
            actionOnTrigger();
        else
            Debug.Log("No action on trigger between turns");
    }

    void IStateTrigger.DisableTrigger()
    {
        Debug.Log("Disabling BetweenTurns Trigger");
        // Nie ma po co wylaczac na mock'upie
    }

    void IStateTrigger.GiveActionOnTrigger(Action actionOnTrigger)
    {
        this.actionOnTrigger = actionOnTrigger;
    }
}
